package helperku

import (
	"fmt"
	"log"
	"strconv"

	"github.com/astaxie/beego"

	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

// BotTele param bot telegram
var BotTele *tgbotapi.BotAPI

func InitTele() {
restartTele:
	var token string
	fmt.Println(beego.BConfig.RunMode)
	if beego.BConfig.RunMode == beego.PROD {
		token = beego.AppConfig.String("tele1::token")
	} else {
		token = beego.AppConfig.String("tele2::token")
	}
	fmt.Println("token tele:" + token)
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = false
	BotTele = bot
	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 10
	exampleQuery := "hello, world"
	exampleCallbackData := "123"
	markup := tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.InlineKeyboardButton{
				Text: "Query",
				SwitchInlineQueryCurrentChat: &exampleQuery,
			},
			tgbotapi.InlineKeyboardButton{
				Text:         "Remove inline keyboard",
				CallbackData: &exampleCallbackData,
			},
		),
	)

	updates, err := bot.GetUpdatesChan(u)
	for update := range updates {
		if update.Message != nil {
			if update.Message.Text == "myid" {
				msgTxt := fmt.Sprintf("%d", update.Message.Chat.ID)
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, msgTxt)
				bot.Send(msg)
			} else if update.Message.Text == "ping" {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
				msg.ReplyMarkup = &markup
				bot.Send(msg)
			} else if update.Message != nil {
				teleParseMsg(update.Message, bot)
			}
			// 	continue
		}
		switch {
		// case update.Message != nil:
		// 	log.Fatal("end")
		// go message(update.Message)
		case update.InlineQuery != nil && len(update.InlineQuery.Query) <= 255: // Just don't update results if query exceeds the maximum length
			log.Fatal("end InlineQuery")
			// go inline(update.InlineQuery)
		case update.ChosenInlineResult != nil:
			log.Fatal("end ChosenInlineResult")
			// go chosenResult(update.ChosenInlineResult)
		case update.CallbackQuery != nil:
			log.Fatal(update.CallbackQuery)
			log.Fatal("end CallbackQuery")
			// go callback(update.CallbackQuery)
		case update.ChannelPost != nil:
			log.Fatal("end ChannelPost")
			// go channelPost(upd.ChannelPost)
		}
		// {
		// 	msgTxt := "Invalid Command"
		// 	msg := tgbotapi.NewMessage(update.Message.Chat.ID, msgTxt)
		// 	bot.Send(msg)
		// }
		// log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
	}
	fmt.Println("exit from telegram")
	goto restartTele
}

// TeleSend
// kirim telegram berdasarkan id dan text
func TeleSend(idTele, msgTxt string) error {
	idTeleInt, err := strconv.Atoi(idTele)
	if err != nil {
		return err
	}
	msg := tgbotapi.NewMessage(int64(idTeleInt), msgTxt)
	_, err = BotTele.Send(msg)
	return err
}
