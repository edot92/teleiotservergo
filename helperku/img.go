package helperku

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"image/gif"
	"image/jpeg"
	"image/png"
	"log"
	"os"
	"strings"

	"github.com/nfnt/resize"
)

var LokasiFile string

func init() {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(dir)
	LokasiFile = dir + "/static/img/user/"
}

// GenerateTempFileImg
func GenerateTempFileImg(data string, nameFile string) error {
	idx := strings.Index(data, ";base64,")
	if idx < 0 {
		return errors.New("InvalidImage")
	}
	ImageType := data[11:idx]

	unbased, err := base64.StdEncoding.DecodeString(data[idx+8:])
	if err != nil {
		return errors.New("Cannot decode b64")
	}
	r := bytes.NewReader(unbased)
	switch ImageType {
	case "png":
		im, err := png.Decode(r)
		if err != nil {
			return errors.New("Bad png")
		}
		f, err := os.OpenFile(LokasiFile+nameFile+".png", os.O_WRONLY|os.O_CREATE, 0777)
		if err != nil {
			return errors.New("Cannot open file png")
		}

		png.Encode(f, im)
	case "jpeg":
		img, err := jpeg.Decode(r)
		// resize
		width := uint(200)
		height := uint(200)
		im := resize.Resize(width, height, img, resize.Lanczos3)
		if err != nil {
			return errors.New("Bad jpeg")
		}

		f, err := os.OpenFile(LokasiFile+nameFile+".jpeg", os.O_WRONLY|os.O_CREATE, 0777)
		if err != nil {
			log.Fatal(err)
			return errors.New("Cannot open file jpeg")
		}
		jpeg.Encode(f, im, nil)
	case "gif":
		im, err := gif.Decode(r)
		if err != nil {
			return errors.New("Bad gif")
		}

		f, err := os.OpenFile(LokasiFile+nameFile+".gif", os.O_WRONLY|os.O_CREATE, 0777)
		if err != nil {
			return errors.New("Cannot open file gif")
		}

		gif.Encode(f, im, nil)
	}
	return nil
}
func DeleteFileTempImg(path string) error {

	return os.Remove(LokasiFile + path + ".jpeg")
}
