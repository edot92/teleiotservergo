package helperku

import (
	"errors"
	"log"
	"strconv"
	"strings"
	"time"

	valid "github.com/asaskevich/govalidator"

	"github.com/globalsign/mgo/bson"
	"github.com/metakeule/fmtdate"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

func init() {

}

func getStringBeetween(source, firstDelimier, endDelimiter string) string {
	tempString := ""
	flagFirst := false
	flagEnd := false
	newString := ""
	for index := 0; index < len(source); index++ {
		if flagFirst == false {
			if string(source[index]) == firstDelimier {
				flagFirst = true
			}
		} else if flagEnd == false && flagFirst == true {
			if string(source[index]) == endDelimiter {
				flagEnd = true
				newString = tempString
			} else {
				tempString += string(source[index])
			}
		}
	}
	return newString
}
func GetListDevice(msgTele *tgbotapi.Message) (listDevice []string, err error) {
	idTeleInt := strconv.Itoa(int(msgTele.Chat.ID))
	mgoSes := DBCopyMGO()
	defer mgoSes.Close()
	var res map[string]interface{}
	var resArray []map[string]interface{}
	// get email pelanggan ref by id telegram
	err = mgoSes.DB(DBNAME).C(ColPelanggan).Find(
		bson.M{
			"idTele": idTeleInt,
		},
	).One(&res)
	if err != nil {
		msgTxt := "id telgram  tidak tersedia"
		return listDevice, errors.New(msgTxt)
	}
	emailnya := res["email"].(string)
	// get device id pelanggan ref by email dan nama alias device
	err = mgoSes.DB(DBNAME).C(ColDevices).Find(
		bson.M{
			"emailPelanggan": emailnya,
		},
	).Select(bson.M{
		"deviceAlias": 1,
	}).All(&resArray)
	if err != nil {
		msgTxt := "anda brlum memiliki daftar perangkat"
		return listDevice, errors.New(msgTxt)
	}
	count := len(resArray)
	for index := 0; index < count; index++ {
		temp1 := resArray[index]
		temp2 := temp1["deviceAlias"].(string)
		listDevice = append(listDevice, temp2)
	}
	return listDevice, nil

}
func teleParseMsg(msgTele *tgbotapi.Message, bot *tgbotapi.BotAPI) error {
	txtInbox := msgTele.Text
	if msgTele.Text == "/start" || msgTele.Text == "start" || msgTele.Text == "4. Pilih Perangkat lain" {
		// get device
		listDevice, err := GetListDevice(msgTele)
		// log.Println(listDevice)

		if err != nil {
			msg := tgbotapi.NewMessage(msgTele.Chat.ID, err.Error())
			bot.Send(msg)
		} else {
			var itemList [][]tgbotapi.KeyboardButton
			count := len(listDevice)
			for index := 0; index < count; index++ {
				itemList = append(itemList,
					tgbotapi.NewKeyboardButtonRow(
						tgbotapi.NewKeyboardButton("$_"+listDevice[index])),
				)
			}

			var numericKeyboard = tgbotapi.NewReplyKeyboard(
				tgbotapi.NewKeyboardButtonRow(
					tgbotapi.NewKeyboardButton("1"),
					tgbotapi.NewKeyboardButton("2"),
					tgbotapi.NewKeyboardButton("3"),
				),
				tgbotapi.NewKeyboardButtonRow(
					tgbotapi.NewKeyboardButton("4"),
					tgbotapi.NewKeyboardButton("5"),
					tgbotapi.NewKeyboardButton("6"),
				),
			)
			msg := tgbotapi.NewMessage(msgTele.Chat.ID, "Pilih Perangkat ")
			msg.ReplyMarkup = tgbotapi.ReplyKeyboardMarkup{
				Keyboard:        itemList,
				Selective:       true,
				ResizeKeyboard:  true,
				OneTimeKeyboard: true,
			}
			bot.Send(msg)
			_ = numericKeyboard
		}

	} else if strings.Contains(txtInbox, "$_") {
		MSGaRRAY := strings.Split(txtInbox, "_")
		namaPerangkat := MSGaRRAY[1]
		log.Println(txtInbox)
		log.Println(MSGaRRAY)
		var customKey = tgbotapi.NewReplyKeyboard(
			tgbotapi.NewKeyboardButtonRow(
				tgbotapi.NewKeyboardButton("1. Cek Daya ["+namaPerangkat+"]"),
			),
			tgbotapi.NewKeyboardButtonRow(
				tgbotapi.NewKeyboardButton("2. Set Batas Daya ["+namaPerangkat+"]"),
			),
			tgbotapi.NewKeyboardButtonRow(
				tgbotapi.NewKeyboardButton("3. Kendali ["+namaPerangkat+"]"),
			),
			tgbotapi.NewKeyboardButtonRow(
				tgbotapi.NewKeyboardButton("4. Pilih Perangkat lain"),
			),
		)
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, "Pilih Perintah ")
		msg.ReplyMarkup = customKey
		bot.Send(msg)
	} else if strings.Contains(txtInbox, "1. Cek Daya") {
		namaPerangkat := strings.Replace(txtInbox, "1. Cek Daya [", "", -1)
		namaPerangkat = strings.Replace(namaPerangkat, "]", "", -1)
		// msg := tgbotapi.NewMessage(msgTele.Chat.ID, " Daya perangkat "+namaPerangkat+" sebesar ")
		// bot.Send(msg)
		// TODO waktu bebas
		waktuAwal := "01-" + fmtdate.Format("MM-YYYY", time.Now())
		waktuAkhir := "31-" + fmtdate.Format("MM-YYYY", time.Now())
		var formatMsg teleFormatCmd1
		formatMsg.NamaPerangkat = namaPerangkat
		formatMsg.WaktuAwal = waktuAwal
		formatMsg.WaktuAkhir = waktuAkhir
		teleCmd1(formatMsg, msgTele.Chat.ID, *bot)
	} else if strings.Contains(txtInbox, "2. Set Batas Daya") {
		namaPerangkat := strings.Replace(txtInbox, "2. Set Batas Daya", "", -1)
		exampleQuery := "daya yang ingin di atur pada perangkat " + namaPerangkat + " adalah "
		// exampleCallbackData := "123"
		markup := tgbotapi.NewInlineKeyboardMarkup(
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.InlineKeyboardButton{
					Text: "set_batas_daya",
					SwitchInlineQueryCurrentChat: &exampleQuery,
				},
				// tgbotapi.InlineKeyboardButton{
				// 	Text: "get_batas_daya",
				// 	SwitchInlineQueryCurrentChat: &exampleQuery,
				// },
			),
		)
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, "Pilihan pengaturan daya ")
		msg.ReplyMarkup = &markup
		bot.Send(msg)
	} else if strings.Contains(txtInbox, "daya yang ingin di atur pada perangkat") {
		msgArray1 := strings.Split(txtInbox, "adalah")
		valueDayaSet := strings.Replace(msgArray1[1], " ", "", -1)
		valueDayaSet = strings.Replace(valueDayaSet, " ", "", -1)
		// lala
		namaPerangkat := getStringBeetween(txtInbox, "[", "]")
		namaPerangkat = strings.Replace(namaPerangkat, " ", "", -1)
		if namaPerangkat == "" && valid.IsInt(valueDayaSet) == false {
			msg := tgbotapi.NewMessage(msgTele.Chat.ID, "invalid command set daya")
			bot.Send(msg)
		} else {
			var msgInput teleFormatCmd2
			msgInput.NamaPerangkat = namaPerangkat
			msgInput.BatasKwh = valueDayaSet
			cmd2SetBatasPemakaianV2(msgInput, msgTele.Chat.ID, bot)
		}
	} else if strings.Contains(txtInbox, "3. Kendali ") {
		namaPerangkat := getStringBeetween(txtInbox, "[", "]")
		namaPerangkat = strings.Replace(namaPerangkat, " ", "", -1)
		// exampleCallbackData := "123"
		matiString := "Matikan Perangkat [" + namaPerangkat + "]"
		nyalaString := "Nyalakan Perangkat [" + namaPerangkat + "]"
		markup := tgbotapi.NewInlineKeyboardMarkup(
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.InlineKeyboardButton{
					Text: "Matikan Perangkat " + namaPerangkat,
					SwitchInlineQueryCurrentChat: &matiString,
				},
			),
			tgbotapi.NewInlineKeyboardRow(

				tgbotapi.InlineKeyboardButton{
					Text: "Nyalakan Perangkat " + namaPerangkat,
					SwitchInlineQueryCurrentChat: &nyalaString,
				},
			),
		)
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, "Pilih Kendali")
		msg.ReplyMarkup = &markup
		bot.Send(msg)
	} else if strings.Contains(txtInbox, "Matikan Perangkat [") {
		var msgInput teleFormatCmd3

		msgInput.NamaPerangkat = getStringBeetween(txtInbox, "[", "]")
		msgInput.Command = "off"
		cmd2OnOffPerangkatV2(msgInput, msgTele.Chat.ID, bot)
		// exampleCallbackData := "123"

	} else if strings.Contains(txtInbox, "Nyalakan Perangkat [") {
		var msgInput teleFormatCmd3

		msgInput.NamaPerangkat = getStringBeetween(txtInbox, "[", "]")
		msgInput.Command = "on"
		cmd2OnOffPerangkatV2(msgInput, msgTele.Chat.ID, bot)
		// exampleCallbackData := "123"

	} else {
		msgTxt := "Invalid Command :" + txtInbox
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
	}

	// else if strings.Contains(txtInbox, "1##") {
	// 	cmd1CekPemakaianListrik(msgTele, bot)
	// } else if strings.Contains(txtInbox, "2##") {
	// 	cmd2SetBatasPemakaian(msgTele, bot)
	// } else if strings.Contains(txtInbox, "3##") {
	// 	cmd2OnOffPerangkat(msgTele, bot)
	// }

	return nil
}

// command 1
/*
func cmd1CekPemakaianListrik(msgTele *tgbotapi.Message, bot *tgbotapi.BotAPI) error {
	txtInbox := msgTele.Text
	txtInboxReplaced := strings.Replace(txtInbox, "1##", "", -1)
	msgArray := strings.Split(txtInboxReplaced, "#")
	if len(msgArray) != 3 {
		msgTxt := "format command 1 tidak sesuai"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)

	}
	namaPerangkat := msgArray[0]
	waktuAwal := msgArray[1]
	waktuAkhir := msgArray[2]

	_, err := fmtdate.Parse("DD-MM-YYYY", waktuAwal)
	if err != nil {
		msgTxt := "format waktu awal tidak sesuai, contoh 1##kantor#20-06-2017#28-06-2017"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	_, err = fmtdate.Parse("DD-MM-YYYY", waktuAkhir)
	if err != nil {
		msgTxt := "format waktu akhir tidak sesuai, contoh 1##kantor#20-06-2017#28-06-2017"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	idTeleInt := strconv.Itoa(int(msgTele.Chat.ID))
	mgoSes := DBCopyMGO()
	defer mgoSes.Close()
	var res map[string]interface{}
	// get email pelanggan ref by id telegram
	err = mgoSes.DB(DBNAME).C(ColPelanggan).Find(
		bson.M{
			"idTele": idTeleInt,
		},
	).One(&res)
	if err != nil {
		msgTxt := "id telgram  tidak tersedia"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	emailnya := res["email"].(string)
	// get device id pelanggan ref by email dan nama alias device
	err = mgoSes.DB(DBNAME).C(ColDevices).Find(
		bson.M{
			"emailPelanggan": emailnya,
			"deviceAlias":    namaPerangkat,
		},
	).One(&res)
	if err != nil {
		msgTxt := "nama perangkat tidak tersedia"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	idDeviceHex := res["_id"].(bson.ObjectId)
	roomID := idDeviceHex.Hex()
	// For our example, suppose we're executing an external
	// call that returns its result on a channel `c1`
	// after 2s.
	c1 := make(chan string, 1)
	go func() {
		log.Println("send " + roomID + " with " + txtInbox)
		Serversocketio.BroadcastTo(roomID, "tele_cmd", txtInbox, func(so socketio.Socket, data string) {
			log.Println("Client ACK with data: ", data)
			c1 <- data
		})
	}()
	select {
	case res := <-c1:
		fmt.Println(res)
		msgTxt := res
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
	case <-time.After(10 * time.Second):
		fmt.Println("timeout")
		msgTxt := "device not response / offline"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
	}

	return nil

}

// command 2
func cmd2SetBatasPemakaian(msgTele *tgbotapi.Message, bot *tgbotapi.BotAPI) error {
	txtInbox := msgTele.Text
	txtInboxReplaced := strings.Replace(txtInbox, "2##", "", -1)
	msgArray := strings.Split(txtInboxReplaced, "#")
	if len(msgArray) != 2 {
		msgTxt := "format command 2 tidak sesuai"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	namaPerangkat := msgArray[0]
	batasKwh := msgArray[1]
	if valid.IsInt(batasKwh) == false {
		msgTxt := "format command 2 tidak sesuai ,nilai batas harus angka"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	idTeleInt := strconv.Itoa(int(msgTele.Chat.ID))
	mgoSes := DBCopyMGO()
	defer mgoSes.Close()
	var res map[string]interface{}
	// get email pelanggan ref by id telegram
	err := mgoSes.DB(DBNAME).C(ColPelanggan).Find(
		bson.M{
			"idTele": idTeleInt,
		},
	).One(&res)
	if err != nil {
		msgTxt := "id telgram  tidak tersedia"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	emailnya := res["email"].(string)
	// get device id pelanggan ref by email dan nama alias device
	err = mgoSes.DB(DBNAME).C(ColDevices).Find(
		bson.M{
			"emailPelanggan": emailnya,
			"deviceAlias":    namaPerangkat,
		},
	).One(&res)
	if err != nil {
		msgTxt := "nama perangkat tidak tersedia"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	idDeviceHex := res["_id"].(bson.ObjectId)
	roomID := idDeviceHex.Hex()
	// For our example, suppose we're executing an external
	// call that returns its result on a channel `c1`
	// after 2s.
	c1 := make(chan string, 1)
	go func() {
		log.Println("send " + roomID + " with " + txtInbox)
		Serversocketio.BroadcastTo(roomID, "tele_cmd", txtInbox, func(so socketio.Socket, data string) {
			log.Println("Client ACK with data: ", data)
			c1 <- data
		})
	}()
	select {
	case res := <-c1:
		fmt.Println(res)
		msgTxt := res
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
	case <-time.After(10 * time.Second):
		fmt.Println("timeout")
		msgTxt := "device not response / offline"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
	}

	return nil

}

// command 3
func cmd2OnOffPerangkat(msgTele *tgbotapi.Message, bot *tgbotapi.BotAPI) error {
	txtInbox := msgTele.Text
	txtInboxReplaced := strings.Replace(txtInbox, "3##", "", -1)
	msgArray := strings.Split(txtInboxReplaced, "#")
	if len(msgArray) != 2 {
		msgTxt := "format command 2 tidak sesuai"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	namaPerangkat := msgArray[0]
	cmdOnOrOff := msgArray[1]
	cmdOnOrOff = strings.ToLower(cmdOnOrOff)
	if cmdOnOrOff != "on" && cmdOnOrOff != "off" {
		msgTxt := "format command 3 tidak sesuai "
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}

	// msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
	// bot.Send(msg)
	// // (room, "browser_cmd", "on")
	// return nil
	idTeleInt := strconv.Itoa(int(msgTele.Chat.ID))
	mgoSes := DBCopyMGO()
	defer mgoSes.Close()
	var res map[string]interface{}
	// get email pelanggan ref by id telegram
	err := mgoSes.DB(DBNAME).C(ColPelanggan).Find(
		bson.M{
			"idTele": idTeleInt,
		},
	).One(&res)
	if err != nil {
		msgTxt := "id telgram  tidak tersedia"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	emailnya := res["email"].(string)
	// get device id pelanggan ref by email dan nama alias device
	err = mgoSes.DB(DBNAME).C(ColDevices).Find(
		bson.M{
			"emailPelanggan": emailnya,
			"deviceAlias":    namaPerangkat,
		},
	).One(&res)
	if err != nil {
		msgTxt := "nama perangkat tidak tersedia"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	idDeviceHex := res["_id"].(bson.ObjectId)
	roomID := idDeviceHex.Hex()
	// For our example, suppose we're executing an external
	// call that returns its result on a channel `c1`
	// after 2s.
	c1 := make(chan string, 1)
	go func() {
		log.Println("send " + roomID + " with " + txtInbox)
		Serversocketio.BroadcastTo(roomID, "tele_cmd", txtInbox, func(so socketio.Socket, data string) {
			log.Println("Client ACK with data: ", data)
			c1 <- data
		})
	}()
	select {
	case res := <-c1:
		fmt.Println(res)
		msgTxt := res

		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
	case <-time.After(10 * time.Second):
		fmt.Println("timeout")
		msgTxt := "device not response / offline"
		msg := tgbotapi.NewMessage(msgTele.Chat.ID, msgTxt)
		bot.Send(msg)
	}

	return nil

}
*/
