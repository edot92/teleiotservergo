package helperku

import (
	"errors"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

var signKey string

func init() {
	signKey = "HARUSNPIYA INI SIGN KEY, TAPI SEMENTARA INI DULU"
}

type MyCustomClaims struct {
	ID    string `json:"id"`
	Email string `json:"email"`
	Role  string `json:"role"`
	jwt.StandardClaims
}

// JWTGenerateToken generate JWT return expireCookie, token string and jika ada error
func JWTGenerateToken(role string, id string, email string) (int64, string, error) {
	mySigningKey := []byte(signKey)
	expireToken := time.Now().Add(time.Hour * 48).Unix()
	// Create the Claims
	claims := MyCustomClaims{
		id,
		email,
		role,
		jwt.StandardClaims{
			ExpiresAt: expireToken,
			Issuer:    email,
			Id:        id,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString(mySigningKey)
	// fmt.Printf("%v %v", ss, err)
	return expireToken, ss, err
}

// JWTCekIsAvail isOk akan false jika gagal dan ada msgErr dalam string
func JWTCekIsAvail(tokenString string) (role string, id, email string, isOk bool, msgErr string) {
	// Token from another example.  This token is expired
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(signKey), nil
	})
	if err != nil {
		return "", "", "", false, err.Error()
	}

	if token.Valid {

	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			// msgErr = ("That's not even a token")
			msgErr = "invalid token"
		} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			// Token is either expired or not active yet
			msgErr = ("timeout token, please login again")
		} else {
			msgErr = "invalid token"
			// msgErr = "Couldn't handle this token:" + err.Error()
		}
	} else {
		msgErr = "invalid token"
		// msgErr = "Couldn't handle this token:" + err.Error()
	}
	if msgErr != "" {
		return "", "", "", false, msgErr
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		email = claims["email"].(string)
		id = claims["id"].(string)
		role = claims["role"].(string)

	} else {
		return "", "", "", false, "failed"
	}
	return role, id, email, true, ""
}

// JWTrefresh refresh token jwt
func JWTrefresh(tokenString string) (newToken string, err error) {
	role, id, email, _, msgErr := JWTCekIsAvail(tokenString)
	if msgErr != "" {
		return "", errors.New(msgErr)
	}
	_, newToken, err = JWTGenerateToken(role, id, email)
	return newToken, err
}
