package helperku

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	valid "github.com/asaskevich/govalidator"
	"github.com/googollee/go-socket.io"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/metakeule/fmtdate"
	tgbotapi "gopkg.in/telegram-bot-api.v4"
)

type teleFormatCmd1 struct {
	NamaPerangkat string `json:"namaPerangkat"`
	WaktuAwal     string `json:"waktuAwal"`
	WaktuAkhir    string `json:"waktuAkhir"`
}
type teleFormatCmd2 struct {
	NamaPerangkat string `json:"namaPerangkat"`
	BatasKwh      string `json:"batasKwh"`
}
type teleFormatCmd3 struct {
	NamaPerangkat string `json:"namaPerangkat"`
	Command       string `json:"command"`
}

// teleCmd1
// Command telegram untuk cek daya
func teleCmd1(msgInput teleFormatCmd1, chatID int64, bot tgbotapi.BotAPI) error {
	namaPerangkat := msgInput.NamaPerangkat
	waktuAwal := msgInput.WaktuAwal
	waktuAkhir := msgInput.WaktuAkhir
	_, err := fmtdate.Parse("DD-MM-YYYY", waktuAwal)
	var isErrorDate = true
	if err != nil {
		msgTxt := "format waktu awal tidak sesuai, contoh 1##kantor#20-06-2017#28-06-2017"
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	_, err = fmtdate.Parse("DD-MM-YYYY", waktuAkhir)
	if err != nil {
		for index2 := 0; index2 < 3; index2++ {
			if index2 == 0 {
				waktuAkhir = "30-" + fmtdate.Format("MM-YYYY", time.Now())
			} else if index2 == 1 {
				waktuAkhir = "29-" + fmtdate.Format("MM-YYYY", time.Now())
			} else {
				waktuAkhir = "28-" + fmtdate.Format("MM-YYYY", time.Now())
			}
			_, err = fmtdate.Parse("DD-MM-YYYY", waktuAkhir)
			if err != nil {
				log.Println(err)
				msgTxt := "format waktu akhir tidak sesuai, contoh 1##kantor#20-06-2017#28-06-2017"
				msg := tgbotapi.NewMessage(chatID, msgTxt)
				bot.Send(msg)
				return errors.New(msgTxt)
			} else {
				isErrorDate = false
				break
			}
		}
		if isErrorDate == true {
			msgTxt := "format waktu akhir tidak sesuai, contoh 1##kantor#20-06-2017#28-06-2017"
			msg := tgbotapi.NewMessage(chatID, msgTxt)
			bot.Send(msg)
			return errors.New(msgTxt)
		} else {
			msgInput.WaktuAkhir = waktuAkhir
		}

	} else {
		isErrorDate = false
	}
	idTeleInt := strconv.Itoa(int(chatID))
	mgoSes := DBCopyMGO()
	defer mgoSes.Close()
	var res map[string]interface{}
	// get email pelanggan ref by id telegram
	err = mgoSes.DB(DBNAME).C(ColPelanggan).Find(
		bson.M{
			"idTele": idTeleInt,
		},
	).One(&res)
	if err != nil {
		msgTxt := "id telgram  tidak tersedia"
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	emailnya := res["email"].(string)
	// log.Println(emailnya)
	log.Println(namaPerangkat)
	// get device id pelanggan ref by email dan nama alias device
	err = nil
	res = nil
	err = mgoSes.DB(DBNAME).C(ColDevices).Find(
		bson.M{
			"emailPelanggan": emailnya,
			"deviceAlias":    namaPerangkat,
		},
	).One(&res)
	// log.Println(res)
	// log.Println(err)
	if err != nil {
		if err == mgo.ErrNotFound {
			msgTxt := "nama perangkat " + namaPerangkat + " tidak tersedia untuk akun email " + emailnya
			msg := tgbotapi.NewMessage(chatID, msgTxt)
			bot.Send(msg)
			return errors.New(msgTxt)
		} else {
			msgTxt := "Kesalahan pada nama perangkat " + namaPerangkat + " Silakan coba beberapa saat lagi"
			msg := tgbotapi.NewMessage(chatID, msgTxt)
			bot.Send(msg)
			return errors.New(msgTxt)
		}

	}
	idDeviceHex := res["_id"].(bson.ObjectId)
	roomID := idDeviceHex.Hex()

	c1 := make(chan string, 1)
	go func() {
		byteInput, _ := json.Marshal(msgInput)
		// log.Println(msgInput.NamaPerangkat)
		// log.Println(msgInput.WaktuAkhir)
		// log.Println(msgInput.WaktuAwal)
		Serversocketio.BroadcastTo(roomID, "tele_cmd_1", msgInput, func(so socketio.Socket, data string) {
			log.Println("Client ACK with data: ", data)
			c1 <- data
			_ = byteInput
		})
	}()
	select {
	case res := <-c1:
		fmt.Println(res)
		msgTxt := res
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
	case <-time.After(10 * time.Second):
		fmt.Println("timeout")
		msgTxt := "device not response / offline"
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
	}

	return nil

}

// cmd2SetBatasPemakaianV2
func cmd2SetBatasPemakaianV2(msgInput teleFormatCmd2, chatID int64, bot *tgbotapi.BotAPI) error {
	namaPerangkat := msgInput.NamaPerangkat
	batasKwh := msgInput.BatasKwh
	if valid.IsInt(batasKwh) == false {
		msgTxt := "format command 2 tidak sesuai ,nilai batas harus angka"
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	idTeleInt := strconv.Itoa(int(chatID))
	mgoSes := DBCopyMGO()
	defer mgoSes.Close()
	var res map[string]interface{}
	// get email pelanggan ref by id telegram
	err := mgoSes.DB(DBNAME).C(ColPelanggan).Find(
		bson.M{
			"idTele": idTeleInt,
		},
	).One(&res)
	if err != nil {
		msgTxt := "id telgram  tidak tersedia"
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	emailnya := res["email"].(string)
	// get device id pelanggan ref by email dan nama alias device
	err = mgoSes.DB(DBNAME).C(ColDevices).Find(
		bson.M{
			"emailPelanggan": emailnya,
			"deviceAlias":    namaPerangkat,
		},
	).One(&res)
	if err != nil {
		msgTxt := "nama perangkat " + namaPerangkat + " tidak tersedia"
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	idDeviceHex := res["_id"].(bson.ObjectId)
	roomID := idDeviceHex.Hex()

	c1 := make(chan string, 1)
	go func() {
		Serversocketio.BroadcastTo(roomID, "tele_cmd_2", msgInput, func(so socketio.Socket, data string) {
			log.Println("Client ACK with data: ", data)
			c1 <- data
		})
	}()
	select {
	case res := <-c1:
		fmt.Println(res)
		msgTxt := res
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
	case <-time.After(10 * time.Second):
		fmt.Println("timeout")
		msgTxt := "device not response / offline"
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
	}

	return nil

}

// command 3
func cmd2OnOffPerangkatV2(msgInput teleFormatCmd3, chatID int64, bot *tgbotapi.BotAPI) error {

	namaPerangkat := msgInput.NamaPerangkat
	cmdOnOrOff := msgInput.Command
	cmdOnOrOff = strings.ToLower(cmdOnOrOff)
	if cmdOnOrOff != "on" && cmdOnOrOff != "off" {
		msgTxt := "format command 3 tidak sesuai "
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	idTeleInt := strconv.Itoa(int(chatID))
	mgoSes := DBCopyMGO()
	defer mgoSes.Close()
	var res map[string]interface{}
	// get email pelanggan ref by id telegram
	err := mgoSes.DB(DBNAME).C(ColPelanggan).Find(
		bson.M{
			"idTele": idTeleInt,
		},
	).One(&res)
	if err != nil {
		msgTxt := "id telgram  tidak tersedia"
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	emailnya := res["email"].(string)
	// get device id pelanggan ref by email dan nama alias device
	err = mgoSes.DB(DBNAME).C(ColDevices).Find(
		bson.M{
			"emailPelanggan": emailnya,
			"deviceAlias":    namaPerangkat,
		},
	).One(&res)
	if err != nil {
		msgTxt := "nama perangkat tidak tersedia"
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
		return errors.New(msgTxt)
	}
	idDeviceHex := res["_id"].(bson.ObjectId)
	roomID := idDeviceHex.Hex()
	c1 := make(chan string, 1)
	go func() {
		Serversocketio.BroadcastTo(roomID, "tele_cmd_3", msgInput, func(so socketio.Socket, data string) {
			log.Println("Client ACK with data: ", data)
			c1 <- data
		})
	}()
	select {
	case res := <-c1:
		fmt.Println(res)
		msgTxt := res

		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
	case <-time.After(10 * time.Second):
		fmt.Println("timeout")
		msgTxt := "device not response / offline"
		msg := tgbotapi.NewMessage(chatID, msgTxt)
		bot.Send(msg)
	}

	return nil

}
