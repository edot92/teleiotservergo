package controllers

import (
	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplName = "index.tpl"
}

// @router /sample [get]
func (c *MainController) Sample() {

	// c.Layout = ""
	c.TplName = "layout/index.tpl" //halaman user
	// c.LayoutSections = make(map[string]string)
	// c.LayoutSections["htmlhead"] = "layout/html_head.tpl"
	// c.LayoutSections["konten"] = "user/index.tpl"
	// c.LayoutSections["Sidebar"] = ""
}
