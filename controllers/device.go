package controllers

import (
	"encoding/json"
	"time"

	"github.com/astaxie/beego"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/jimlawless/whereami"
	"gitlab.com/edot92/teleiotservergo/helperku"
)

type SchemaDevice struct {
	ID             bson.ObjectId `json:"_id" bson:"_id"`
	HardwareID     string        `valid:"required" json:"hardwareId" bson:"hardwareId"`
	EmailPelanggan string        `valid:"required" json:"emailPelanggan" bson:"emailPelanggan"`
	DeviceAlias    string        `json:"deviceAlias" bson:"deviceAlias"`
	GeneratedBy    string        `json:"emailPetugasGenerated" bson:"emailPetugasGenerated"`
	CreatedAt      time.Time     `json:"createdAt" bson:"createdAt"`
	UpdateAt       time.Time     `json:"updateAt" bson:"updateAt"`
}

// DeviceController ...
type DeviceController struct {
	beego.Controller
}

// AddNew ...
// @router /auth/addnew/:jwt [get]
func (bRouter *DeviceController) AddNew(jwt string) {
	var err error
	var paramJSON SchemaDevice
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	s := mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices)

	paramJSON.ID = bson.NewObjectId()
	paramJSON.GeneratedBy = email
	paramJSON.CreatedAt = time.Now()
	paramJSON.UpdateAt = time.Now()
	err = s.Insert(paramJSON)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}

	bRouter.Data["json"] = map[string]interface{}{
		"error":  0,
		"msgErr": "",
		"msg":    "success genereate id Device",
		"payload": map[string]interface{}{
			"idDevice": paramJSON.ID.Hex(),
		},
		// "line":    ,
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
	_ = email
}

// @router /auth/GetlistPagination/:offset/:limit/:jwt [get]
func (bRouter *DeviceController) GetlistPagination(offset, limit int, jwt string) {
	var err error
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	var res []map[string]interface{}
	s := mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices)
	err = s.Find(
		bson.M{},
	).Skip(offset).Limit(limit).Iter().All(&res)
	totalCount, _ := s.Count()
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"payload": map[string]interface{}{
			"devices":    res,
			"totalItems": totalCount,
		},
		"msgErr": "",
		"msg":    "success get data devices",
		// "line":    ,
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
	_ = email
}

// AddNeDeviceaddToPelanggan ...
// menambhaknn perangkat id ke user
// id harus blm terpakai oleh user lain
// @router /auth/DeviceaddToPelanggan/:jwt [post]
func (bRouter *DeviceController) DeviceaddToPelanggan(jwt string) {
	var err error
	type paramBody struct {
		DeviceID    string `json:"deviceID" bson:"deviceID"`
		DeviceAlias string `json:"deviceAlias" bson:"deviceAlias"`
	}
	var paramJSON paramBody
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	err = json.Unmarshal(bRouter.Ctx.Input.RequestBody, &paramJSON)

	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"msg":   "error parameter, code : - " + err.Error(),
			"line":  whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}

	isOk := bson.IsObjectIdHex(paramJSON.DeviceID)
	if isOk == false {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msg":    "format device id tidak sesuai , silakan periksa kembali kode device ID anda. kemudian coba lagi",
			"msgErr": "DeviceID:" + paramJSON.DeviceID,
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	s := mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices)
	idDevice := bson.ObjectIdHex(paramJSON.DeviceID)
	// cek apakah id sudah tersedia
	cnt, err := s.Find(bson.M{
		"_id": idDevice,
	}).Count()
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	if cnt == 0 {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"msg":   "device id tidak ditemukan, silakan periksa kembali kode device ID anda. kemudian coba lagi",
			"line":  whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	// cek apakah sudah terdaftar dengan email lain
	cnt, err = s.Find(bson.M{
		"_id":            idDevice,
		"emailPelanggan": "",
	}).Count()
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	if cnt == 0 {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"msg":   "device sudah dipakai di akun lain",
			"line":  whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	var paramSave SchemaDevice
	paramSave.EmailPelanggan = email
	paramSave.CreatedAt = time.Now()
	paramSave.UpdateAt = time.Now()
	// Update
	colQuerier := bson.M{
		"_id": idDevice,
	}
	change := bson.M{"$set": bson.M{
		"emailPelanggan": email,
		"deviceAlias":    paramJSON.DeviceAlias,
		"updateAt":       time.Now(),
	},
	}
	err = s.Update(colQuerier, change)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}

	bRouter.Data["json"] = map[string]interface{}{
		"error":  0,
		"msgErr": "",
		"msg":    "success membuat device",
		// "line":    ,
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
}

// @router /auth/GetDeviceListPelangganPagination/:offset/:limit/:jwt [get]
func (bRouter *DeviceController) GetDeviceListPelangganPagination(offset, limit int, jwt string) {
	var err error
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	var res []map[string]interface{}
	s := mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices)
	err = s.Find(
		bson.M{
			"emailPelanggan": email,
		},
	).Skip(offset).Limit(limit).Iter().All(&res)
	totalCount, _ := s.Count()
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"payload": map[string]interface{}{
			"devices":    res,
			"totalItems": totalCount,
		},
		"msgErr": "",
		"msg":    "success get data devices",
		// "line":    ,
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
	_ = email
}

// GetAllDeviceByEmailuser
// mengambil data id untuk socket io
// @router /auth/GetAllDeviceByEmailuser/:jwt [get]
func (bRouter *DeviceController) GetAllDeviceByEmailuser(jwt string) {
	var err error
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	var res []map[string]interface{}
	s := mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices)
	err = s.Find(
		bson.M{
			"emailPelanggan": email,
		},
	).Select(
		bson.M{
			"deviceAlias": 1,
			"_id":         1,
		},
	).All(&res)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"payload": map[string]interface{}{
			"devices": res,
		},
		"msgErr": "",
		"msg":    "success get data devices",
		// "line":    ,
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
	_ = email
}

// UpdateDeviceByPelanggan
// update nama id device oleh pelanggan
// @router /auth/UpdateDeviceByPelanggan/:jwt [post]
func (bRouter *DeviceController) UpdateDeviceByPelanggan(jwt string) {
	var err error
	type paramBody struct {
		ID                    string    `json:"_id"`
		CreatedAt             time.Time `json:"createdAt"`
		DeviceAlias           string    `json:"deviceAlias"`
		EmailPelanggan        string    `json:"emailPelanggan"`
		EmailPetugasGenerated string    `json:"emailPetugasGenerated"`
		HardwareID            string    `json:"hardwareId"`
		UpdateAt              time.Time `json:"updateAt"`
		Index                 int       `json:"index"`
	}
	var paramJSON paramBody
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	err = json.Unmarshal(bRouter.Ctx.Input.RequestBody, &paramJSON)

	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"msg":   "error parameter, code : - " + err.Error(),
			"line":  whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}

	isOk := bson.IsObjectIdHex(paramJSON.ID)
	if isOk == false {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msg":    "format device id tidak sesuai , silakan periksa kembali kode device ID anda. kemudian coba lagi",
			"msgErr": "DeviceID:" + paramJSON.ID,
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	colMgo := mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices)
	idDevice := bson.ObjectIdHex(paramJSON.ID)
	// cek apakah id sudah tersedia
	// Update
	colQuerier := bson.M{"_id": idDevice,
		"emailPelanggan": email,
	}
	change := bson.M{"$set": bson.M{"hardwareId": paramJSON.HardwareID, "deviceAlias": paramJSON.DeviceAlias, "updateAt": time.Now()}}
	err = colMgo.Update(colQuerier, change)
	if err != nil {
		if err == mgo.ErrNotFound {
			bRouter.Data["json"] = map[string]interface{}{
				"error":  1,
				"msgErr": "",
				"msg":    "Id perangkat " + paramJSON.ID + " tidak di temukan untuk akun " + email,
				"line":   whereami.WhereAmI(),
			}
			bRouter.ServeJSON()
			return
		}
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error":  0,
		"msgErr": "",
		"msg":    "berhasil update perangkat",
		"line":   whereami.WhereAmI(),
	}
	bRouter.ServeJSON()
	return
	_ = role
	_ = id
	_ = email
	_ = err
}

// BuatDeviceBaruPelanggan
// Buat ID BSON untuk device
// @router /auth/BuatDeviceBaruPelanggan/:jwt [get]
func (bRouter *DeviceController) BuatDeviceBaruPelanggan(jwt string) {
	var err error

	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}

	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	colMgo := mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices)
	idDevices := bson.NewObjectId()
	err = colMgo.Insert(bson.M{
		"email":                 email,
		"_id":                   idDevices,
		"hardwareId":            "-",
		"emailPelanggan":        email,
		"deviceAlias":           "",
		"emailPetugasGenerated": "",
		"createdAt":             time.Now(),
		"updateAt":              time.Now(),
	})
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error":  0,
		"msgErr": "",
		"msg":    "berhasil update perangkat",
		"line":   whereami.WhereAmI(),
	}
	bRouter.ServeJSON()
	return
	_ = role
	_ = id
	_ = email
	_ = err
}

// UpdateDeviceByPetugas
// @router /auth/UpdateDeviceByPetugas/:jwt [post]
func (bRouter *DeviceController) UpdateDeviceByPetugas(jwt2 string) {
	type paramBody struct {
		DeviceAlias    string `json:"deviceAlias" bson:"deviceAlias"`
		HardwareId     string `json:"hardwareId" bson:"hardwareId"`
		DeviceId       string `json:"_id" bson:"_id"`
		EmailPelanggan string `json:"emailPelanggan" bson:"emailPelanggan"`
	}
	var paramJSON paramBody
	jwt := bRouter.Ctx.Request.Header.Get("budijwt")
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	err := json.Unmarshal(bRouter.Ctx.Input.RequestBody, &paramJSON)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "failed parameter",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	if bson.IsObjectIdHex(paramJSON.DeviceId) == false {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"msg":   "failed parameter device id",
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	deviceIDBson := bson.ObjectIdHex(paramJSON.DeviceId)
	var res map[string]interface{}
	// get  device alias
	err = mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices).
		Find(bson.M{
			"_id": deviceIDBson,
		}).One(&res)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "failed update " + err.Error(),
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	// jika namanya beda, dek apakah device alias namanya sudah di pakai
	if res["deviceAlias"] != paramJSON.DeviceAlias {
		cnt, _ := mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices).
			Find(bson.M{
				"_id":         deviceIDBson,
				"deviceAlias": paramJSON.DeviceAlias,
			}).Count()
		if cnt > 0 {
			bRouter.Data["json"] = map[string]interface{}{
				"error": 1,
				"msg":   "failed update " + "Device Alias Sudah tersedia",
			}
			bRouter.ServeJSON()
			return
		}
	}

	colQuerier := bson.M{
		"_id": deviceIDBson,
	}
	change := bson.M{"$set": bson.M{
		"emailPelanggan": paramJSON.EmailPelanggan,
		"hardwareId":     paramJSON.HardwareId,
		"deviceAlias":    paramJSON.DeviceAlias,
	}}
	err = mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices).Update(colQuerier, change)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "failed update " + err.Error(),
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"msg":   "success update",
	}
	bRouter.ServeJSON()
	return
	_ = role
	_ = id
	_ = email
}

// DeleteDeviceByPetugas
// @router /auth/DeleteDeviceByPetugas/:jwt [post]
func (bRouter *DeviceController) DeleteDeviceByPetugas(jwt2 string) {
	type paramBody struct {
		DeviceId string `json:"_id" bson:"_id"`
	}
	var paramJSON paramBody
	jwt := bRouter.Ctx.Request.Header.Get("budijwt")
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" || role != "petugas" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	err := json.Unmarshal(bRouter.Ctx.Input.RequestBody, &paramJSON)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "failed parameter",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	if bson.IsObjectIdHex(paramJSON.DeviceId) == false {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"msg":   "failed parameter device id",
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	deviceIDBson := bson.ObjectIdHex(paramJSON.DeviceId)
	err = mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices).
		Remove(bson.M{
			"_id": deviceIDBson,
		})
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "failed delete " + err.Error(),
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"msg":   "success delete",
	}
	bRouter.ServeJSON()
	// get  device alias
	/*
		err = mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices).
			Find(bson.M{
				"_id": deviceIDBson,
			}).One(&res)
		if err != nil {
			bRouter.Data["json"] = map[string]interface{}{
				"error":  1,
				"msgErr": err.Error(),
				"msg":    "failed update " + err.Error(),
				// "line":    ,
			}
			bRouter.ServeJSON()
			return
		}
		// jika namanya beda, dek apakah device alias namanya sudah di pakai
		if res["deviceAlias"] != paramJSON.DeviceAlias {
			cnt, _ := mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices).
				Find(bson.M{
					"_id":         deviceIDBson,
					"deviceAlias": paramJSON.DeviceAlias,
				}).Count()
			if cnt > 0 {
				bRouter.Data["json"] = map[string]interface{}{
					"error": 1,
					"msg":   "failed update " + "Device Alias Sudah tersedia",
				}
				bRouter.ServeJSON()
				return
			}
		}

		colQuerier := bson.M{
			"_id": deviceIDBson,
		}
		change := bson.M{"$set": bson.M{
			"emailPelanggan": paramJSON.EmailPelanggan,
			"hardwareId":     paramJSON.HardwareId,
			"deviceAlias":    paramJSON.DeviceAlias,
		}}
		err = mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices).Update(colQuerier, change)
		if err != nil {
			bRouter.Data["json"] = map[string]interface{}{
				"error":  1,
				"msgErr": err.Error(),
				"msg":    "failed update " + err.Error(),
				// "line":    ,
			}
			bRouter.ServeJSON()
			return
		}
		bRouter.Data["json"] = map[string]interface{}{
			"error": 0,
			"msg":   "success update",
		}
		bRouter.ServeJSON()
	*/
	return
	_ = role
	_ = id
	_ = email
}
