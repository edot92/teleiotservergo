package controllers

import (
	"encoding/json"
	"time"

	"github.com/asaskevich/govalidator"

	"github.com/jimlawless/whereami"
	"gitlab.com/edot92/teleiotservergo/helperku"

	"github.com/astaxie/beego"
	"github.com/globalsign/mgo/bson"
)

type SchemaPetugas struct {
	ID          bson.ObjectId `json:"_id" bson:"_id"`
	NamaLengkap string        `valid:"required" json:"namaLengkap" bson:"namaLengkap"`
	Email       string        `valid:"email,required" json:"email" bson:"email"`
	Password    string        `valid:"required" json:"password" bson:"password"`
	Alamat      string        `json:"alamat" bson:"alamat"`
	Notelp      string        `json:"noTelp" bson:"noTelp"`
	CreatedAt   time.Time     `json:"createdAt" bson:"createdAt"`
	UpdateAt    time.Time     `json:"updateAt" bson:"updateAt"`
}

func init() {

}

type PetugasController struct {
	beego.Controller
}

// '/userdaftar/:username/:password'
// '/login/:username/:password'
// '/refreshjwt/:username/:jwt'
// '/petugas/getlist/:offset/:limit/:jwt'
// '/petugas/addnew/:jwt'
// '/pelanggan/getlist/:offset/:limit/:jwt'
// @router /login/:username/:password [get]
func (bRouter *PetugasController) Login(username, password string) {
	var err error
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	var res map[string]interface{}
	err = mgoSes.DB(helperku.DBNAME).C(helperku.ColPetugas).Find(
		bson.M{
			"email": username,
		},
	).One(&res)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"payload": map[string]interface{}{
				"user": map[string]interface{}{
					"username": "",
					"id":       "",
					"role":     "",
				},
				"jwt": "",
			},
			"msgErr": err.Error(),
			"msg":    "akun tidak tersedia",
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	passwordDb := res["password"].(string)
	if passwordDb != password {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"payload": map[string]interface{}{
				"user": map[string]interface{}{
					"username": "",
					"id":       "",
					"role":     "",
				},
				"jwt": "",
			},
			"msgErr": "",
			"msg":    "password tidak sesuai",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	email := res["email"].(string)
	id := res["_id"].(bson.ObjectId)
	idString := id.String()
	role := "petugas"
	_, jwt, _ := helperku.JWTGenerateToken(
		role,
		idString,
		email,
	)
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"payload": map[string]interface{}{
			"jwt":  jwt,
			"role": role,
		},
		"msgErr": "",
		"msg":    "berhasil login sebagai petugas",
		// "line":    ,
	}
	bRouter.ServeJSON()
}

// GetlistPagination
// respon :
// totalItems
// user
// @router /auth/GetlistPagination/:offset/:limit/:jwt [get]
func (bRouter *PetugasController) GetlistPagination(offset, limit int, jwt string) {
	var err error

	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	var res []map[string]interface{}
	s := mgoSes.DB(helperku.DBNAME).C(helperku.ColPetugas)
	err = s.Find(
		bson.M{},
	).
		Select(
			bson.M{
				"password": 0,
			},
		).Skip(offset).Limit(limit).Iter().All(&res)
	totalCount, _ := s.Count()
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"payload": map[string]interface{}{
			"user":       res,
			"totalItems": totalCount,
		},
		"msgErr": "",
		"msg":    "success get data petugas",
		// "line":    ,
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
	_ = email
}

// AddNew user create petugas
// respon :
// totalItems
// user
// @router /auth/addnew/:jwt [post]
func (bRouter *PetugasController) AddNew(jwt string) {
	var err error
	var paramJSON SchemaPetugas
	err = json.Unmarshal(bRouter.Ctx.Input.RequestBody, &paramJSON)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"msg":   "error parameter, code : - " + err.Error(),
			"line":  whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	_, err = govalidator.ValidateStruct(paramJSON)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msg":    "Harap lengkapi pengisian form",
			"msgErr": err.Error,
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()

	s := mgoSes.DB(helperku.DBNAME).C(helperku.ColPetugas)
	cnt, _ := s.Find(bson.M{
		"email": paramJSON.Email,
	}).Count()
	if cnt > 0 {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": "",
			"msg":    "email sudah tersedia",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	paramJSON.ID = bson.NewObjectId()
	paramJSON.CreatedAt = time.Now()
	paramJSON.UpdateAt = time.Now()
	err = s.Insert(paramJSON)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error":  0,
		"msgErr": "",
		"msg":    "success membuat akun petugas",
		// "line":    ,
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
	_ = email
}

// @router /auth/GetProfile/:jwt [get]
func (bRouter *PetugasController) GetProfile(offset, limit int, jwt string) {
	var err error
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	var res map[string]interface{}
	s := mgoSes.DB(helperku.DBNAME).C(helperku.ColPetugas)
	err = s.Find(
		bson.M{
			"email": email,
		},
	).
		Select(
			bson.M{
				"password": 0,
			},
		).One(&res)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"payload": map[string]interface{}{
			"user": res,
		},
		"msgErr": "",
		"msg":    "success get data Petugas",
		// "line":    ,
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
	_ = email
}
