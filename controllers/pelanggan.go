package controllers

import (
	"encoding/json"
	"time"

	"github.com/metakeule/fmtdate"

	"github.com/asaskevich/govalidator"
	"github.com/astaxie/beego"
	"github.com/globalsign/mgo/bson"
	"github.com/jimlawless/whereami"
	"gitlab.com/edot92/teleiotservergo/helperku"
)

// SchemaPelanggan
type SchemaPelanggan struct {
	ID          bson.ObjectId `json:"_id" bson:"_id"`
	NamaLengkap string        `valid:"required" json:"namaLengkap" bson:"namaLengkap"`
	Password    string        `valid:"required" json:"password" bson:"password"`
	Email       string        `valid:"required,email" json:"email" bson:"email"`
	Alamat      string        `valid:"required" json:"alamat" bson:"alamat"`
	NoTelp      string        `valid:"required" json:"noTelp" bson:"noTelp"`
	IDTele      string        `valid:"required" json:"idTele" bson:"idTele"`
	CreatedAt   time.Time     `json:"createdAt" bson:"createdAt"`
	UpdateAt    time.Time     `json:"updateAt" bson:"updateAt"`
}

func init() {

}

// PelangganController ...
type PelangganController struct {
	beego.Controller
}

// @router /login/:username/:password [get]
func (bRouter *PelangganController) Login(username, password string) {
	var err error
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	var res map[string]interface{}
	err = mgoSes.DB(helperku.DBNAME).C(helperku.ColPelanggan).Find(
		bson.M{
			"email": username,
		},
	).One(&res)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"payload": map[string]interface{}{
				"user": map[string]interface{}{
					"username": "",
					"id":       "",
					"role":     "",
				},
				"jwt": "",
			},
			"msgErr": err.Error(),
			"msg":    "akun tidak tersedia",
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	passwordDb := res["password"].(string)
	if passwordDb != password {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"payload": map[string]interface{}{
				"user": map[string]interface{}{
					"username": "",
					"id":       "",
					"role":     "",
				},
				"jwt": "",
			},
			"msgErr": "",
			"msg":    "password tidak sesuai",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	email := res["email"].(string)
	id := res["_id"].(bson.ObjectId)
	idString := id.String()
	role := "pelanggan"
	_, jwt, _ := helperku.JWTGenerateToken(
		role,
		idString,
		email,
	)
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"payload": map[string]interface{}{
			"jwt":  jwt,
			"role": role,
		},
		"msgErr": "",
		"msg":    "berhasil login sebagai pelanggan",
		// "line":    ,
	}
	bRouter.ServeJSON()
}

// @router /addnew/:jwt [post]
func (bRouter *PelangganController) AddNew(jwt string) {
	var err error
	var paramJSON SchemaPelanggan
	err = json.Unmarshal(bRouter.Ctx.Input.RequestBody, &paramJSON)

	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"msg":   "error parameter, code : - " + err.Error(),
			"line":  whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}

	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	_, err = govalidator.ValidateStruct(paramJSON)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msg":    "Harap lengkapi pengisian form",
			"msgErr": err.Error(),
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	s := mgoSes.DB(helperku.DBNAME).C(helperku.ColPelanggan)
	cnt, err := s.Find(bson.M{
		"email": paramJSON.Email,
	}).Count()
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			"line":   "",
		}
		bRouter.ServeJSON()
		return
	}
	if cnt > 0 {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": "",
			"msg":    "email sudah tersedia",
			"line":   whereami.WhereAmI(),
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	cnt, _ = s.Find(bson.M{
		"idTele": paramJSON.IDTele,
	}).Count()
	if cnt > 0 {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": "",
			"msg":    "id telegram sudah digunakan",
			"line":   whereami.WhereAmI(),
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	paramJSON.ID = bson.NewObjectId()
	paramJSON.CreatedAt = time.Now()
	paramJSON.UpdateAt = time.Now()
	err = s.Insert(paramJSON)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			"line":   whereami.WhereAmI(),
		}
		bRouter.ServeJSON()
		return
	}

	bRouter.Data["json"] = map[string]interface{}{
		"error":  0,
		"msgErr": "",
		"msg":    "success membuat akun Pelanggan",
		// "line":    ,
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
	_ = email
}

// @router /auth/GetlistPagination/:offset/:limit/:jwt [get]
func (bRouter *PelangganController) GetlistPagination(offset, limit int, jwt string) {
	var err error

	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	var res []map[string]interface{}
	s := mgoSes.DB(helperku.DBNAME).C(helperku.ColPelanggan)
	err = s.Find(
		bson.M{},
	).
		Select(
			bson.M{
				"password": 0,
			},
		).Skip(offset).Limit(limit).Iter().All(&res)
	totalCount, _ := s.Count()
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"payload": map[string]interface{}{
			"user":       res,
			"totalItems": totalCount,
		},
		"msgErr": "",
		"msg":    "success get data pelanggan",
		// "line":    ,
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
	_ = email
}

// @router /auth/GetProfile/:jwt [get]
func (bRouter *PelangganController) GetProfile(offset, limit int, jwt string) {
	var err error
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	var res map[string]interface{}
	s := mgoSes.DB(helperku.DBNAME).C(helperku.ColPelanggan)
	err = s.Find(
		bson.M{
			"email": email,
		},
	).
		Select(
			bson.M{
				"password": 0,
			},
		).One(&res)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan, silakan coba lagi beberapa saat kembali",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"payload": map[string]interface{}{
			"user": res,
		},
		"msgErr": "",
		"msg":    "success get data pelanggan",
		// "line":    ,
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
	_ = email
}

// mengirim telegram dari web
// @router /auth/sendtelegram/:namaLengkap/:idTelegram/:jwt [get]
func (bRouter *PelangganController) Sendtelegram(namaLengkap, idTelegram, jwt string) {

	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	waktuNow := fmtdate.Format("hh:mm:ss DD-MM-YYYY", time.Now())
	txtMSg := "Hai " + namaLengkap + " , ini adalah pesan dari aplikasi telegram iot \n" + "dikirim pada " + waktuNow
	err := helperku.TeleSend(idTelegram, txtMSg)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "terjadi sedikit kesalahan , code " + err.Error(),
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"msg":   "silakan periksa kotak masuk telegram anda",
	}
	bRouter.ServeJSON()
	_ = role
	_ = id
	_ = email
}

// @router /auth/UpdateDevicePelanggan [post]
func (bRouter *PelangganController) UpdateDevicePelanggan() {
	type paramBody struct {
		DeviceAlias string `json:"deviceAlias" bson:"deviceAlias"`
		HardwareId  string `json:"hardwareId" bson:"hardwareId"`
		DeviceId    string `json:"_id" bson:"_id"`
	}
	var paramJSON paramBody
	jwt := bRouter.Ctx.Request.Header.Get("budijwt")
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	err := json.Unmarshal(bRouter.Ctx.Input.RequestBody, &paramJSON)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "failed parameter",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	if bson.IsObjectIdHex(paramJSON.DeviceId) == false {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"msg":   "failed parameter device id",
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	deviceIDBson := bson.ObjectIdHex(paramJSON.DeviceId)
	var res map[string]interface{}
	// get  device alias
	err = mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices).
		Find(bson.M{
			"emailPelanggan": email,
			"_id":            deviceIDBson,
		}).One(&res)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "failed update " + err.Error(),
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	// jika namanya beda, dek apakah device alias namanya sudah di pakai
	if res["deviceAlias"] != paramJSON.DeviceAlias {
		cnt, _ := mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices).
			Find(bson.M{
				"emailPelanggan": email,
				"_id":            deviceIDBson,
				"deviceAlias":    paramJSON.DeviceAlias,
			}).Count()
		if cnt > 0 {
			bRouter.Data["json"] = map[string]interface{}{
				"error": 1,
				"msg":   "failed update " + "Device Alias Sudah tersedia",
			}
			bRouter.ServeJSON()
			return
		}
	}

	colQuerier := bson.M{
		"emailPelanggan": email,
		"_id":            deviceIDBson,
	}
	change := bson.M{"$set": bson.M{
		"hardwareId":  paramJSON.HardwareId,
		"deviceAlias": paramJSON.DeviceAlias,
	}}
	err = mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices).Update(colQuerier, change)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "failed update " + err.Error(),
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"msg":   "success update",
	}
	bRouter.ServeJSON()
	return
	_ = role
	_ = id
	_ = email
}

// @router /auth/Updateprofile [post]
func (bRouter *PelangganController) Updateprofile() {
	type paramBody struct {
		ID          string    `json:"_id"`
		Alamat      string    `json:"alamat"`
		CreatedAt   time.Time `json:"createdAt"`
		Email       string    `json:"email"`
		IDTele      string    `json:"idTele"`
		NamaLengkap string    `json:"namaLengkap"`
		NoTelp      string    `json:"noTelp"`
		UpdateAt    time.Time `json:"updateAt"`
	}
	var paramJSON paramBody
	jwt := bRouter.Ctx.Request.Header.Get("budijwt")
	role, id, email, _, msgErr := helperku.JWTCekIsAvail(jwt)
	if msgErr != "" {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  2,
			"msgErr": msgErr,
			"msg":    "failed token",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	err := json.Unmarshal(bRouter.Ctx.Input.RequestBody, &paramJSON)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "failed parameter",
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	if bson.IsObjectIdHex(paramJSON.ID) == false {
		bRouter.Data["json"] = map[string]interface{}{
			"error": 1,
			"msg":   "failed parameter  id",
		}
		bRouter.ServeJSON()
		return
	}
	mgoSes := helperku.DBCopyMGO()
	defer mgoSes.Close()
	idPelangganBson := bson.ObjectIdHex(paramJSON.ID)
	colQuerier := bson.M{
		"_id":   idPelangganBson,
		"email": email,
	}
	change := bson.M{"$set": bson.M{
		"namaLengkap": paramJSON.NamaLengkap,
		"alamat":      paramJSON.Alamat,
		"noTelp":      paramJSON.NoTelp,
		"idTele":      paramJSON.IDTele,
		"updateAt":    time.Now(),
	}}
	err = mgoSes.DB(helperku.DBNAME).C(helperku.ColPelanggan).Update(colQuerier, change)
	if err != nil {
		bRouter.Data["json"] = map[string]interface{}{
			"error":  1,
			"msgErr": err.Error(),
			"msg":    "failed update " + err.Error(),
			// "line":    ,
		}
		bRouter.ServeJSON()
		return
	}
	bRouter.Data["json"] = map[string]interface{}{
		"error": 0,
		"msg":   "success update",
	}
	bRouter.ServeJSON()
	return
	_ = role
	_ = id
	_ = email
}
