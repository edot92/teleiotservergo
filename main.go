package main

import (
	"fmt"
	"os"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/edot92/teleiotservergo/helperku"
	_ "gitlab.com/edot92/teleiotservergo/routers"
)
import _ "github.com/asaskevich/govalidator"

func main() {
	// beego.BConfig.WebConfig.StaticDir["/static"] = "/static/"
	beego.SetStaticPath("/api/static", "static")

	beego.BConfig.WebConfig.TemplateLeft = "<<<"
	beego.BConfig.WebConfig.TemplateRight = ">>>"
	beego.BConfig.WebConfig.AutoRender = false

	beego.BConfig.CopyRequestBody = true
	beego.BConfig.EnableErrorsShow = true
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log := logs.NewLogger()
	log.SetLogger(logs.AdapterConsole)
initDb:
	sesMgo, err := helperku.InitDB()
	if err != nil {
		time.Sleep(5 * time.Second)
		goto initDb
	}
	defer sesMgo.Close()
	go func() {
		for {
			// cek looping untuk ping server
			time.Sleep(5 * time.Second)
			err = sesMgo.Ping()
			if err != nil {
				fmt.Println(err)
				// jika ada error / network tidak reposn , coba konek kembali
				sesMgo, err = helperku.InitDB()
				if err != nil {
					fmt.Println(err)
				}
			}
		}
	}()
	go func() {
		helperku.InitTele()
	}()
	name, _ := os.Hostname()
	fmt.Println("HOSTNAME:" + name)
	if name != "edot92-E430" {
		beego.BConfig.RunMode = "prod"
	}

	beego.Run(":" + port)
}
