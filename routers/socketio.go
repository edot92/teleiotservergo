package routers

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/astaxie/beego"
	"github.com/globalsign/mgo/bson"
	"github.com/googollee/go-socket.io"
	"gitlab.com/edot92/teleiotservergo/helperku"
)

type structEnergyMeter struct {
	Token      string `json:"token"`
	Tegangan   string `json:"tegangan"`
	HardwareID string `json:"hardwareId"`
	Arus       string `json:"arus"`
	DayaAktif  string `json:"dayaAktif"`
	Daya       string `json:"daya"`
	Frekuensi  string `json:"frekuensi"`
}

func initSocketio() {
	// Configure websocket route
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}
	server.On("connection", func(so socketio.Socket) {
		var deviceID string
		// log.Println("on connection")
		// so.Join("chat")
		so.Join("quest")
		so.On("server_join_room", func(msg string) {
			// room =deviceID
			so.Join(msg)
			so.Leave("quest")
			deviceID = msg
			fmt.Println("clint device want join to room " + msg)

		})
		so.On("browser_cmd_on", func(msg string) {
			room := msg
			server.BroadcastTo(room, "browser_cmd", "on")
		})
		so.On("browser_cmd_off", func(msg string) {
			room := msg
			server.BroadcastTo(room, "browser_cmd", "off")
		})
		so.On("browser_ping_dari_alat", func(msg string) {
			// log.Println(msg)
			// log.Println("browser_ping_dari_alat")
			deviceID2 := msg
			server.BroadcastTo("quest", deviceID2+"_online", time.Now())
		})
		// get id telegram berdasarkan kepemilikan alat
		// 1 get email user col device
		// 2 get id elgram d colpelanaggan
		so.On("notif_dari_alat", func(msg string) {
			// msg
			// splitArray
			log.Println("notif dari alat")
			// log.Println(msg)

			msgArray := strings.Split(msg, "(-_-)")
			if len(msgArray) == 2 {

				deviceID = msgArray[0]
				msgDariAlat := msgArray[1]

				var res map[string]interface{}
				var res2 map[string]interface{}
				// get  device alias
				mgoSes := helperku.DBCopyMGO()
				defer mgoSes.Close()
				deviceIDBson := bson.ObjectIdHex(deviceID)
				err = mgoSes.DB(helperku.DBNAME).C(helperku.ColDevices).
					Find(bson.M{
						"_id": deviceIDBson,
					}).One(&res)
				if err != nil {
					log.Println("er deviceIDBson : " + deviceIDBson.Hex())
					log.Println(err)
					return
				}
				// log.Println(res)
				// setelah dapat email dapatkan id telegram, jika ada
				// jika namanya beda, dek apakah device alias namanya sudah di pakai
				err = mgoSes.DB(helperku.DBNAME).C(helperku.ColPelanggan).Find(bson.M{
					"emailPelanggan": res["emailPelanggan"],
				}).One(&res2)
				if err != nil {
					log.Println("email not found")
					log.Println(err)
					return
				}
				idTelestr := res2["idTele"].(string)
				if idTelestr == "" {
					return
				}
				namaPerangkat := res["deviceAlias"].(string)
				msgDariAlat = "Informasi dari perangkat " + namaPerangkat + " ." + msgDariAlat
				helperku.TeleSend(idTelestr, msgDariAlat)
			} else {
				log.Println("email not found")
				return
			}
		})

		so.On("update_modbus", func(msg string) {
			var tempData structEnergyMeter
			err1 := json.Unmarshal([]byte(msg), &tempData)
			if err1 == nil {
				roomID := "quest_browser_" + tempData.Token
				// fmt.Println(roomID)
				// server.BroadcastTo(roomID, msg)
				server.BroadcastTo("quest", roomID, msg)
				server.BroadcastTo("quest", deviceID+"_online", time.Now())
				// server.BroadcastTo("browser_"+tempData.Token, msg)
			} else {
				fmt.Println(err1)
			}
		})
		server.BroadcastTo("quest", "quest_tes", "alalalla")
		so.On("disconnection", func() {
			if deviceID != "" {
				// log.Println("broadcast closed to " + deviceID)
				server.BroadcastTo("quest", deviceID+"_close", time.Now())
			} else {
				fmt.Println("on disconnect")
			}
		})

	})
	server.On("error", func(so socketio.Socket, err error) {
		log.Println("error:", err)
	})
	beego.Handler("/socket.io/", server)
	// stop := interval.Start(func() {
	// fmt.Println("Hello, World !")
	// a := server.GetMaxConnection()
	// server.BroadcastTo("chat", "chat message", "kikuk")
	// fmt.Println(a)
	// }, 1*time.Second)
	helperku.Serversocketio = server
	// _ = stop
	// Start listening for incoming chat messages
	// go handleMessages()

	//setup http server
	// serveMux := http.NewServeMux()
	// serveMux.Handle("/socket.io/", server)
}
