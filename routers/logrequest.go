package routers

import (
	"encoding/json"
	"log"
	"strings"

	"github.com/astaxie/beego/context"

	"github.com/astaxie/beego"
)

func init() {
	var FilterUser = func(ctx *context.Context) {
		// userAgent := ctx.Input.UserAgent()
		// var isOk bool
		// if strings.Contains(userAgent, "PostmanRuntime") {
		// 	isOk = true
		// }
		// fmt.Println(userAgent)
		// if isOk == false {
		// 	res := map[string]interface{}{
		// 		"error": 4,
		// 		"msg":   "hayoloh mau ngapain",
		// 	}
		// 	byteRes, _ := json.Marshal(&res)
		// 	ctx.ResponseWriter.Write(byteRes)
		// 	return
		// }
		if strings.Contains(ctx.Input.URL(), "/auth/") == false {
			// log.Println("tidak ada auth")
			return
		}
		// jika url mengandung api auth , maka harus ada jwt
		jwtStr := ctx.Input.Header("budijwt")
		if jwtStr == "" {
			res := map[string]interface{}{
				"error": 2,
				"msg":   "invalid header",
			}
			log.Println(res)
			byteRes, _ := json.Marshal(&res)
			ctx.ResponseWriter.Write(byteRes)
			return
		}
	}

	beego.InsertFilter("/api/*", beego.BeforeRouter, FilterUser)

	// beego.InsertFilter("/*", beego.FinishRouter, func(ctx *context.Context) {
	// 	fmt.Println(ctx.Input.UserAgent())
	// }, false)
}
