// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"github.com/astaxie/beego"
	"gitlab.com/edot92/teleiotservergo/controllers"
)

func init() {
	// beego.ErrorController(&controllers.ErrorController{})
	ns := beego.NewNamespace("/api",
		beego.NSNamespace("/petugas",
			beego.NSInclude(
				&controllers.PetugasController{},
			),
		), beego.NSNamespace("/pelanggan",
			beego.NSInclude(
				&controllers.PelangganController{},
			),
		), beego.NSNamespace("/device",
			beego.NSInclude(
				&controllers.DeviceController{},
			),
		),
	)
	initSocketio()
	beego.AddNamespace(ns)
}
