package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "BuatDeviceBaruPelanggan",
			Router: `/auth/BuatDeviceBaruPelanggan/:jwt`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "DeleteDeviceByPetugas",
			Router: `/auth/DeleteDeviceByPetugas/:jwt`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(
				param.New("jwt2"),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "DeviceaddToPelanggan",
			Router: `/auth/DeviceaddToPelanggan/:jwt`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "GetAllDeviceByEmailuser",
			Router: `/auth/GetAllDeviceByEmailuser/:jwt`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "GetDeviceListPelangganPagination",
			Router: `/auth/GetDeviceListPelangganPagination/:offset/:limit/:jwt`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("offset", param.InPath),
				param.New("limit", param.InPath),
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "GetlistPagination",
			Router: `/auth/GetlistPagination/:offset/:limit/:jwt`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("offset", param.InPath),
				param.New("limit", param.InPath),
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "UpdateDeviceByPelanggan",
			Router: `/auth/UpdateDeviceByPelanggan/:jwt`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "UpdateDeviceByPetugas",
			Router: `/auth/UpdateDeviceByPetugas/:jwt`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(
				param.New("jwt2"),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:DeviceController"],
		beego.ControllerComments{
			Method: "AddNew",
			Router: `/auth/addnew/:jwt`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:MainController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:MainController"],
		beego.ControllerComments{
			Method: "Sample",
			Router: `/sample`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"],
		beego.ControllerComments{
			Method: "AddNew",
			Router: `/addnew/:jwt`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"],
		beego.ControllerComments{
			Method: "GetProfile",
			Router: `/auth/GetProfile/:jwt`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("offset"),
				param.New("limit"),
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"],
		beego.ControllerComments{
			Method: "GetlistPagination",
			Router: `/auth/GetlistPagination/:offset/:limit/:jwt`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("offset", param.InPath),
				param.New("limit", param.InPath),
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"],
		beego.ControllerComments{
			Method: "UpdateDevicePelanggan",
			Router: `/auth/UpdateDevicePelanggan`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"],
		beego.ControllerComments{
			Method: "Updateprofile",
			Router: `/auth/Updateprofile`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"],
		beego.ControllerComments{
			Method: "Sendtelegram",
			Router: `/auth/sendtelegram/:namaLengkap/:idTelegram/:jwt`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("namaLengkap", param.InPath),
				param.New("idTelegram", param.InPath),
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PelangganController"],
		beego.ControllerComments{
			Method: "Login",
			Router: `/login/:username/:password`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("username", param.InPath),
				param.New("password", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PetugasController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PetugasController"],
		beego.ControllerComments{
			Method: "GetProfile",
			Router: `/auth/GetProfile/:jwt`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("offset"),
				param.New("limit"),
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PetugasController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PetugasController"],
		beego.ControllerComments{
			Method: "GetlistPagination",
			Router: `/auth/GetlistPagination/:offset/:limit/:jwt`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("offset", param.InPath),
				param.New("limit", param.InPath),
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PetugasController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PetugasController"],
		beego.ControllerComments{
			Method: "AddNew",
			Router: `/auth/addnew/:jwt`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(
				param.New("jwt", param.InPath),
			),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PetugasController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/teleiotservergo/controllers:PetugasController"],
		beego.ControllerComments{
			Method: "Login",
			Router: `/login/:username/:password`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(
				param.New("username", param.InPath),
				param.New("password", param.InPath),
			),
			Params: nil})

}
