package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/plugins/cors"
)

func init() {
	// isCors, _ := beego.AppConfig.Bool("cors")
	// if isCors == true {
	beego.InsertFilter("/*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowOrigins: []string{

			"https://telkombot.herokuapp.com",
			"http://telkombot.herokuapp.com",
			"http://localhost:8000",
			"http://*:*",
			"http://localhost:3000",
		},
		AllowMethods: []string{"PUT", "PATCH", "POST", "OPTIONS", "DELETE"},
		AllowHeaders: []string{"_xsrf",
			"budijwt",
			"X-CsrfToken", "Content-Type", "Origin", "Access-Control-Allow-Origin"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}))
	// }
}
